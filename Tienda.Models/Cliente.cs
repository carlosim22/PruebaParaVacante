﻿using System;
using System.Collections.Generic;

namespace Tienda.Models
{
    public partial class Cliente
    {
        public Cliente()
        {
            Venta = new HashSet<Venta>();
        }

        public int IdCliente { get; set; }
        public string? Nombre { get; set; }
        public string? Apellidos { get; set; }
        public string? Domicilio { get; set; }
        public string? Correo { get; set; }
        public string? Contrasenia { get; set; }
        public bool? ActivoInactivo { get; set; }

        public virtual ICollection<Venta> Venta { get; set; }
    }
}
