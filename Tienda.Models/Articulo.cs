﻿using System;
using System.Collections.Generic;

namespace Tienda.Models
{
    public partial class Articulo
    {
        public Articulo()
        {
            VentasArticulos = new HashSet<VentasArticulo>();
        }

        public int IdArticulos { get; set; }
        public int? IdTienda { get; set; }
        public string? Codigo { get; set; }
        public string? Nombre { get; set; }
        public string? Descripcion { get; set; }
        public float? Precio { get; set; }
        public string? Imagen { get; set; }
        public float? Stock { get; set; }
        public bool? ActivoInactivo { get; set; }

        public virtual Tiendum? IdTiendaNavigation { get; set; }
        public virtual ICollection<VentasArticulo> VentasArticulos { get; set; }
    }
}
