﻿using System;
using System.Collections.Generic;

namespace Tienda.Models
{
    public partial class Venta
    {
        public Venta()
        {
            VentasArticulos = new HashSet<VentasArticulo>();
        }

        public int IdVenta { get; set; }
        public int? IdCliente { get; set; }
        public DateTime? FechaVenta { get; set; }
        public float? Subtotal { get; set; }
        public float? Total { get; set; }

        public virtual Cliente? IdClienteNavigation { get; set; }
        public virtual ICollection<VentasArticulo> VentasArticulos { get; set; }
    }
}
