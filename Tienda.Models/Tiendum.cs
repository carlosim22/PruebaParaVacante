﻿using System;
using System.Collections.Generic;

namespace Tienda.Models
{
    public partial class Tiendum
    {
        public Tiendum()
        {
            Articulos = new HashSet<Articulo>();
        }

        public int IdTienda { get; set; }
        public string? Sucursal { get; set; }
        public string? Direccion { get; set; }
        public bool? ActivoInactivo { get; set; }

        public virtual ICollection<Articulo> Articulos { get; set; }
    }
}
