﻿using System;
using System.Collections.Generic;

namespace Tienda.Models
{
    public partial class VentasArticulo
    {
        public int IdVentaArticulos { get; set; }
        public int? IdVenta { get; set; }
        public int? IdArticulo { get; set; }
        public float? Precio { get; set; }
        public float? Cantidad { get; set; }

        public virtual Articulo? IdArticuloNavigation { get; set; }
        public virtual Venta? IdVentaNavigation { get; set; }
    }
}
