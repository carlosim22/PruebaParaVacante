﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tienda.DAL.DataContext;
using Tienda.Models;

namespace Tienda.DAL.Repositorio
{
    public class ClientesRepository : IGenericRepository<Cliente>
    {
        private readonly TIENDAContext _context;

        public ClientesRepository(TIENDAContext context) { 
            _context = context;
        }

        public async Task<bool> Delete(int id)
        {
            Cliente tienda = _context.Clientes.First(x => x.IdCliente == id);
            tienda.ActivoInactivo = false;
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<Cliente> Get(int id)
        {
            return await _context.Clientes.FindAsync(id);
        }

        public async Task<IQueryable<Cliente>> GetAll()
        {
            IQueryable<Cliente> queryTiendaSql = _context.Clientes;

            return queryTiendaSql;
        }

        public async Task<bool> Insert(Cliente modelo)
        {
            _context.Clientes.Add(modelo);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<bool> Update(Cliente modelo)
        {
            _context.Clientes.Update(modelo);
            await _context.SaveChangesAsync();
            return true;
        }
    }
}
