﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tienda.DAL.DataContext;
using Tienda.Models;

namespace Tienda.DAL.Repositorio
{
    public class VentasRepository : IGenericRepository<Venta>
    {
        private readonly TIENDAContext _context;

        public VentasRepository(TIENDAContext context)
        {
            _context = context;
        }

        public async Task<bool> Delete(int id)
        {
            Venta venta = _context.Ventas.First(x => x.IdVenta == id);
            _context.Ventas.Remove(venta);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<Venta> Get(int id)
        {
            return await _context.Ventas.FindAsync(id);
        }

        public async Task<IQueryable<Venta>> GetAll()
        {
            IQueryable<Venta> queryTiendaSql = _context.Ventas;

            return queryTiendaSql;
        }

        public async Task<bool> Insert(Venta modelo)
        {
            _context.Ventas.Add(modelo);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<bool> Update(Venta modelo)
        {
            _context.Ventas.Update(modelo);
            await _context.SaveChangesAsync();
            return true;
        }
    }
}
