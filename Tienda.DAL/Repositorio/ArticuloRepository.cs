﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tienda.DAL.DataContext;
using Tienda.Models;

namespace Tienda.DAL.Repositorio
{
    public class ArticuloRepository : IGenericRepository<Articulo>
    {
        private readonly TIENDAContext _context;

        public ArticuloRepository(TIENDAContext context)
        {
            _context = context;
        }

        public async Task<bool> Delete(int id)
        {
            Articulo tienda = _context.Articulos.First(x => x.IdArticulos == id);
            tienda.ActivoInactivo = false;
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<Articulo> Get(int id)
        {
            return await _context.Articulos.FindAsync(id);
        }

        public async Task<IQueryable<Articulo>> GetAll()
        {
            IQueryable<Articulo> queryTiendaSql = _context.Articulos;

            return queryTiendaSql;
        }

        public async Task<bool> Insert(Articulo modelo)
        {
            _context.Articulos.Add(modelo);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<bool> Update(Articulo modelo)
        {
            _context.Articulos.Update(modelo);
            await _context.SaveChangesAsync();
            return true;
        }
    }
}
