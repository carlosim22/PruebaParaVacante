﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tienda.DAL.DataContext;
using Tienda.Models;

namespace Tienda.DAL.Repositorio
{
    public class TiendaRepository: IGenericRepository<Tiendum>
    {
        private readonly TIENDAContext _context;

        public TiendaRepository(TIENDAContext context)
        {
            _context = context;
        }

        public async Task<bool> Delete(int id)
        {
            Tiendum tienda = _context.Tienda.First(x => x.IdTienda == id);
            tienda.ActivoInactivo = false;
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<Tiendum> Get(int id)
        {
            return await _context.Tienda.FindAsync(id);
        }

        public async Task<IQueryable<Tiendum>> GetAll()
        {
            IQueryable<Tiendum> queryTiendaSql = _context.Tienda;

            return queryTiendaSql;
        }

        public async Task<bool> Insert(Tiendum modelo)
        {
            _context.Tienda.Add(modelo);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<bool> Update(Tiendum modelo)
        {
            _context.Tienda.Update(modelo);
            await _context.SaveChangesAsync();
            return true;
        }
    }
}
