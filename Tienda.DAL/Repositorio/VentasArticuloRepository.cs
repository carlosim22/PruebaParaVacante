﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tienda.DAL.DataContext;
using Tienda.Models;

namespace Tienda.DAL.Repositorio
{
    public class VentasArticuloRepository : IGenericRepository<VentasArticulo>
    {
        private readonly TIENDAContext _context;

        public VentasArticuloRepository(TIENDAContext context)
        {
            _context = context;
        }

        public async Task<bool> Delete(int id)
        {
            VentasArticulo articulo = _context.VentasArticulos.First(x => x.IdVentaArticulos == id);
            _context.VentasArticulos.Remove(articulo);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<VentasArticulo> Get(int id)
        {
            return await _context.VentasArticulos.FindAsync(id);
        }

        public async Task<IQueryable<VentasArticulo>> GetAll()
        {
            IQueryable<VentasArticulo> queryTiendaSql = (IQueryable<VentasArticulo>)_context.VentasArticulos;

            return queryTiendaSql;
        }

        public async Task<bool> Insert(VentasArticulo modelo)
        {
            _context.VentasArticulos.Add(modelo);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<bool> Update(VentasArticulo modelo)
        {
            _context.VentasArticulos.Update(modelo);
            await _context.SaveChangesAsync();
            return true;
        }
    }
}
