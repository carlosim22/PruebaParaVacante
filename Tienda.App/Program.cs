using Microsoft.EntityFrameworkCore;
using Tienda.BLL.Service;
using Tienda.DAL.DataContext;
using Tienda.DAL.Repositorio;
using Tienda.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Text.Json.Serialization;

var MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddCors(options =>
{
    options.AddPolicy(name: MyAllowSpecificOrigins,
                      policy =>
                      {
                          policy.WithOrigins("*").AllowAnyHeader().WithMethods("*");
                      });
});

builder.Services.AddControllersWithViews();

builder.Services.AddDbContext<TIENDAContext>(options =>
{
    options.UseSqlServer(builder.Configuration.GetConnectionString("conexionSql"));
});

builder.Services.AddScoped<IGenericRepository<Tiendum>, TiendaRepository>();
builder.Services.AddScoped<ITiendaService, TiendaService>();

builder.Services.AddScoped<IGenericRepository<Articulo>, ArticuloRepository>();
builder.Services.AddScoped<IArticuloService, ArticuloService>();

builder.Services.AddScoped<IGenericRepository<Cliente>, ClientesRepository>();
builder.Services.AddScoped<IClientesService, ClientesService>();

builder.Services.AddScoped<IGenericRepository<Venta>, VentasRepository>();
builder.Services.AddScoped<IVentasService, VentasService>();

builder.Services.AddScoped<IGenericRepository<VentasArticulo>, VentasArticuloRepository>();
builder.Services.AddScoped<IVentasArticulosService, VentasArticulosService>();

builder.Services.AddControllers().AddJsonOptions(x =>
                x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
{
    options.RequireHttpsMetadata = false;
    options.SaveToken = true;
    options.TokenValidationParameters = new TokenValidationParameters()
    {
        ValidateIssuer = true,
        ValidateAudience = true,
        ValidateLifetime = true,
        ValidateIssuerSigningKey = true,
        ValidAudience = builder.Configuration["Jwt:Audience"],
        ValidIssuer = builder.Configuration["Jwt:Issuer"],
        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration["Jwt:Key"]))
    };
});


var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseRouting();

app.UseCors(MyAllowSpecificOrigins);

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller}/{action=Index}/{id?}");

app.MapFallbackToFile("index.html");

app.Run();
