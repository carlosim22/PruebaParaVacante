import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: any;

  constructor() { 
    this.getUser();
  }

  login(user: any){
    this.user = user;
    localStorage.setItem('user', JSON.stringify(user));
  }

  getUser(){
    this.user = JSON.parse(localStorage.getItem('user') || '[]');
    console.log(this.user);
  }

  unlogin(){
    this.user = [];
    localStorage.removeItem('user');
  }


}
