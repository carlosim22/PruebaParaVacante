import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CarritoService {

  public carrito: any = [];

  constructor() { 
    this.getCarrito();
  }

  agregarProducto(producto: any){
    let existe: boolean = false;

    if (this.carrito != 'null') {
      this.carrito.forEach((element: any) => {
        if (element.IdArticulo == producto.IdArticulo) {
          if (producto.stock > element.cantidad + 1) {
            element.cantidad++;
          }
          existe = true;
        }
      });
    } else {
      this.carrito = [];
    }

    if (!existe) {
      this.carrito.push(producto);
    }

    localStorage.setItem('carrito', JSON.stringify(this.carrito));
  }

  eliminarCantidadProducto(producto: any) {

    if (this.carrito != 'null') {
      this.carrito.forEach((element: any, index: any) => {
        if (element.IdArticulo == producto.IdArticulo) {
          element.cantidad--;
          if (element.cantidad == 0) {
            this.eliminarProducto(index);
          }
        }
      });
    }

    localStorage.setItem('carrito', JSON.stringify(this.carrito));
  }

  getCarrito() {
    this.carrito = JSON.parse(localStorage.getItem('carrito') || '[]');
  }

  eliminarProducto(index: number){
    this.carrito.splice(index, 1);
    localStorage.setItem('carrito', this.carrito);
  }

  eliminarCarrito(){
    this.carrito = [];
    localStorage.removeItem('carrito');
  }

}
