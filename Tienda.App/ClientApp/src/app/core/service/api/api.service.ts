import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

import { AuthService } from '../auth/auth.service';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  baseUrl: string = environment.URL;

  private readonly headers = new HttpHeaders({
    'Authorization': `Bearer ${this.auth.user }`
  });

  private readonly requestOptions = { headers: this.headers };

  constructor(private http: HttpClient, private auth: AuthService) { }

  //EMPIEZA USUARIOS
  login(params: any){
    return this.http.post(`${this.baseUrl}Login`, params, { responseType: 'text' });
  }
  registro(params: any){
    return this.http.post(`${this.baseUrl}Clientes`, params);
  }
  //TERMINA USUARIOS

  // EMPIEZA TIENDAS
  obtenerTiendas() {
    return this.http.get(`${this.baseUrl}Tienda`);
  }

  insertarTiendas(params: any) {
    return this.http.post(`${this.baseUrl}Tienda`, params)
  }

  actulizarTiendas(params: any) {
    return this.http.put(`${this.baseUrl}Tienda`, params)
  }

  eliminarTiendas(id: any) {
    return this.http.delete(`${this.baseUrl}Tienda/${id}`)
  }
  // TERMINA TIENDAS

  // EMPIEZA ARTICULOS
  obtenerArticulos(id_tienda: any) {
    return this.http.get(`${ this.baseUrl }Articulos?id_tienda=${id_tienda}`)
  }

  insertarArticulo(params: any) {
    return this.http.post(`${this.baseUrl}Articulos`, params)
  }

  actulizarArticulo(params: any) {
    return this.http.put(`${this.baseUrl}Articulos`, params)
  }

  eliminarArticulo(id: any) {
    return this.http.delete(`${this.baseUrl}Articulos/${id}`)
  }
  // EMPIEZA ARTICULOS

  //EMPIEZA VENTAS
  obtenerVentas(id_cliente: any) {
    return this.http.get(`${this.baseUrl}Ventas?id_cliente=${id_cliente}`);
  }

  insertarVentas(params: any) {
    return this.http.post(`${this.baseUrl}Ventas`, params);
  }

  obtenerVentasArticulos(id_venta: any) {
    return this.http.get(`${this.baseUrl}VentasArticulos?id_venta=${id_venta}`);
  }

  insertarVentasArticulos(params: any) {
    return this.http.post(`${this.baseUrl}VentasArticulos`, params);
  }
  //TERMINA VENTAS
}
