import { Component, OnInit } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../../../core/service/api/api.service';
import { CarritoService } from '../../../core/service/carrito/carrito.service';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {

  tiendas: any = [];
  baseImg: string = environment.UrlImage;

  constructor(private api: ApiService, private carrito: CarritoService) { }

  ngOnInit(): void {
    this.obtenerTiendas();
  }

  obtenerTiendas() {
    this.api.obtenerTiendas().subscribe((res: any) => {
      this.tiendas = res;
      this.obtenerProductos();
    });
  }

  obtenerProductos() {
    this.tiendas.forEach((element: any) => {
      this.api.obtenerArticulos(element.idTienda).subscribe((res: any) => {
        element.Productos = res;
      });
    });
  }

  agregarProducto(producto: any) {
    let data = {
      IdArticulo: producto.idArticulos,
      nombre: producto.nombre,
      imagen: this.baseImg + producto.imagen,
      cantidad: 1,
      stock: producto.stock,
      precio: producto.precio
    }

    this.carrito.agregarProducto(data);

    Swal.fire({
      icon: 'success',
      text: 'Producto agregado al carrito'
    });
  }
}
