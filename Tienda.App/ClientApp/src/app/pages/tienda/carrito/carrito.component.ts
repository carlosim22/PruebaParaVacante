import Swal from 'sweetalert2'
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../../core/service/api/api.service';
import { AuthService } from '../../../core/service/auth/auth.service';
import { CarritoService } from '../../../core/service/carrito/carrito.service';

@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.component.html',
  styleUrls: ['./carrito.component.css']
})
export class CarritoComponent implements OnInit {

  productos: any = [];
  total: number = 0;

  constructor(public carrito: CarritoService, private route: Router, private api: ApiService, private auth: AuthService) {
    this.productos = carrito.carrito;
  }

  ngOnInit(): void {
    this.obtenerTotal();
  }

  obtenerTotal() {
    this.total = 0;
    this.carrito.carrito.forEach((element: any) => {
      this.total += (element.precio * element.cantidad);
    });
  }

  agregarProductos(data: any) {
    this.carrito.agregarProducto(data);
    this.obtenerTotal();
  }

  eliminarProductoCantidad(data: any) {
    this.carrito.eliminarCantidadProducto(data);
    this.obtenerTotal();
  }

  eliminarProductos(index: any) {
    this.carrito.eliminarProducto(index);
    this.obtenerTotal();
  }

  guardarVenta() {
    let data = {
      IdCliente: JSON.parse(this.auth.user).idCliente,
      Subtotal: this.total,
      Total: this.total
    }

    this.api.insertarVentas(data).subscribe((res: any) => {
      let productos: any[] = [];
      this.carrito.carrito.forEach((element: any) => {
        productos.push({
          IdArticulo: element.IdArticulo,
          IdVenta: res.idVenta,
          Precio: element.precio,
          Cantidad: element.cantidad
        });
      });

      this.api.insertarVentasArticulos(productos).subscribe((res: any) => {
        console.log(res);
        Swal.fire({
          icon: 'success',
          text: 'Venta realizada'
        }).then(() => {
          this.carrito.eliminarCarrito();
          this.route.navigateByUrl("/tienda");
        });
      });
    });
  }
}
