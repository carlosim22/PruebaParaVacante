import { Component, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { InicioComponent } from './inicio/inicio.component';
import { ProductosComponent } from './productos/productos.component';
import { CarritoComponent } from './carrito/carrito.component';
import { VentasComponent } from './ventas/ventas.component';

import { DxDataGridModule, DxPopupModule, DxTemplateModule } from 'devextreme-angular';

const routes: Routes = [
  { path: '', component: InicioComponent },
  { path: 'productos/:id_tienda', component: ProductosComponent},
  { path: 'carrito', component: CarritoComponent},
  { path: 'ventas', component: VentasComponent}
];

@NgModule({
  declarations: [
    InicioComponent,
    ProductosComponent,
    CarritoComponent,
    VentasComponent
  ],
  imports: [
    CommonModule,
    DxDataGridModule,
    DxTemplateModule,
    DxPopupModule,
    RouterModule.forChild(routes)
  ]
})
export class TiendaModule { }
