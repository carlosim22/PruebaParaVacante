import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../core/service/api/api.service';
import { AuthService } from '../../../core/service/auth/auth.service';

@Component({
  selector: 'app-ventas',
  templateUrl: './ventas.component.html',
  styleUrls: ['./ventas.component.css']
})
export class VentasComponent implements OnInit {

  dataSource: any = [];

  popupVisible: boolean = false;
  prodcutosVentas: any = [];

  calculateCellValue(data: any) {
    return data.precio * data.cantidad;
  }

  constructor(private api: ApiService, private auth: AuthService) {
    this.VerArticulos = this.VerArticulos.bind(this);
  }

  ngOnInit(): void {
    this.obtenerVentas();
  }

  obtenerVentas() {
    this.api.obtenerVentas(JSON.parse(this.auth.user).idCliente).subscribe((res: any) => {
      this.dataSource = res;
    });
  }

  VerArticulos(event: any) {
    this.api.obtenerVentasArticulos(event.row.key).subscribe((res: any) => {
      console.log(res);
      this.prodcutosVentas = res;
      this.popupVisible = true;
    });
  }
}
