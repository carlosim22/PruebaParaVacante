import Swal from 'sweetalert2'
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/core/service/api/api.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  registroform: FormGroup;

  constructor(public form: FormBuilder, private api: ApiService, private route: Router) { 
    this.registroform = this.crearFormulario();
  }

  ngOnInit(): void {
  }

  crearFormulario(){
    return this.form.group({
      nombre: ['', [Validators.required]],
      apellidos: ['', [Validators.required]],
      domicilio: ['', [Validators.required]],
      correo: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$" )]],
      contrasenia: ['',[Validators.required, Validators.minLength(3)]],
      activoInactivo: [true]
    });
  }

  registro(){
    let data = this.registroform.value;

    this.api.registro(data).subscribe((res: any) => {
      Swal.fire({
        icon: 'success',
        text: 'Te has registrado con exito'
      }).then(() => {
        this.route.navigateByUrl('/');
      });
    });
  }


}
