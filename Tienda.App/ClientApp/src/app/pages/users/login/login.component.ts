import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/core/service/api/api.service';
import { AuthService } from 'src/app/core/service/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginform: FormGroup;
  type: string = "password";  

  constructor(
    public form: FormBuilder,
    private api: ApiService,
    private auth: AuthService,
    private route: Router) { 
    this.loginform = this.crearformulario();
  }

  ngOnInit(): void {
  }

  get LoginForm(){
    return this.loginform.controls;
  }

  crearformulario(){
    return this.form.group({
      user: ['',[Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$" )]],
      password: ['',[Validators.required, Validators.minLength(3)]],
    });
  }

  changeInput(){
    if (this.type == "password") {
      this.type = "text";
    }else{
      this.type = "password";
    }
  }

  login(){
    let data = this.loginform.value;
    this.api.login(data).subscribe((res: any) => {
      this.auth.login(res);
      this.route.navigateByUrl('/tienda');
    });
  }

}
