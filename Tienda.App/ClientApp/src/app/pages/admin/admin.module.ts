import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './tiendas/admin.component';
import { ArticulosComponent } from './articulos/articulos.component'

import { DxDataGridModule, DxPopupModule, DxTemplateModule } from 'devextreme-angular';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
  { path: '', component: AdminComponent },
  { path: 'articulos/:id', component: ArticulosComponent }
];

@NgModule({
  declarations: [
    AdminComponent,
    ArticulosComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    DxPopupModule,
    DxDataGridModule,
    DxTemplateModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ]
})
export class AdminModule { }
