import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/core/service/api/api.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  tiendaForm: FormGroup;
  dataSource: any = [];

  titleModal: string = "Insertar Tienda";
  popupVisible: boolean = false;
  idTienda: number = 0;

  constructor(public form: FormBuilder, private api: ApiService, private route: Router) {
    this.tiendaForm = this.crearFormulario();
    this.agregarArticulos = this.agregarArticulos.bind(this);
  }

  crearFormulario() {
    return this.form.group({
      sucursal: ['', [Validators.required]],
      direccion: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.cargarContenido();
  }

  cargarContenido() {
    this.api.obtenerTiendas().subscribe((res: any) => {
      this.dataSource = res;
    });
  }

  abrirModal() {
    this.titleModal = "Insertar Tienda";
    this.popupVisible = true;
  }

  actualizarDatos(event: any) {
    this.tiendaForm.controls['sucursal'].setValue(event.data.sucursal);
    this.tiendaForm.controls['direccion'].setValue(event.data.direccion);
    this.idTienda = event.key;

    this.titleModal = "Actualizar " + event.data.sucursal;
    this.popupVisible = true;

    event.cancel = true;
  }

  finalizarFormulario() {
    if (!this.idTienda || this.idTienda == 0) {
      this.agregarTienda();
    } else {
      this.actualizarTienda();
    }
  }

  agregarTienda() {
    let data = this.tiendaForm.value;
    this.api.insertarTiendas(data).subscribe((res: any) => {
      this.tiendaForm.reset();
      this.popupVisible = false;
      this.cargarContenido();
    });
  }

  actualizarTienda() {
    let data = this.tiendaForm.value;
    data.idTienda = this.idTienda;

    this.api.actulizarTiendas(data).subscribe((res: any) => {
      this.tiendaForm.reset();
      this.idTienda = 0;
      this.popupVisible = false;
      this.cargarContenido();
    });
  }

  eliminarTiendas(event: any) {
    this.api.eliminarTiendas(event.key).subscribe((res: any) => {
      this.cargarContenido();
    });
  }

  agregarArticulos(event: any) {
    // console.log(event);
    this.route.navigateByUrl('/admin/articulos/' + event.row.key);
  }
}
