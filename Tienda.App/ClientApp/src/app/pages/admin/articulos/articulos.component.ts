import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/app/core/service/api/api.service';

import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-articulos',
  templateUrl: './articulos.component.html',
  styleUrls: ['./articulos.component.css']
})
export class ArticulosComponent implements OnInit {

  articulosForm: FormGroup;
  id: any;
  dataSource: any = [];

  titleModal: string = "Insertar Tienda";
  popupVisible: boolean = false;
  idArticulo: any;

  image: any;
  image_mostrar: any;

  constructor(public form: FormBuilder, private route: ActivatedRoute,
    private api: ApiService, private sanitizer: DomSanitizer) {
    this.articulosForm = this.crearFormulario();
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.id = params.get('id');
      this.cargarContenido();
    });

  }

  crearFormulario() {
    return this.form.group({
      codigo: ['', [Validators.required]],
      nombre: ['', [Validators.required]],
      descripcion: ['', [Validators.required]],
      precio: ['', [Validators.required]],
      stock: ['', [Validators.required]],
    });
  }

  cargarContenido() {
    this.api.obtenerArticulos(this.id).subscribe((res: any) => {
      this.dataSource = res;
    });
  }

  abrirModal() {
    this.titleModal = "Insertar Tienda";
    this.popupVisible = true;
  }

  actualizarDatos(event: any) {
    this.articulosForm.controls['codigo'].setValue(event.data.codigo);
    this.articulosForm.controls['nombre'].setValue(event.data.nombre);
    this.articulosForm.controls['descripcion'].setValue(event.data.descripcion);
    this.articulosForm.controls['precio'].setValue(event.data.precio);
    this.articulosForm.controls['stock'].setValue(event.data.stock);
    this.idArticulo = event.key;

    this.titleModal = "Actualizar " + event.data.sucursal;
    this.popupVisible = true;

    event.cancel = true;
  }

  subirImagen(event: any) {
    this.image = event.target.files[0];

    this.extraerBase64(this.image).then((imagen: any) => {
      this.image_mostrar = imagen.base;
    });
  }

  extraerBase64 = async ($event: any) => new Promise((resolve, reject) => {
    try {
      const unsafeImg = window.URL.createObjectURL($event);
      const image = this.sanitizer.bypassSecurityTrustUrl(unsafeImg);
      const reader = new FileReader();
      reader.readAsDataURL($event);
      reader.onload = () => {
        resolve({
          base: reader.result
        });
      };
      reader.onerror = () => {
        resolve({
          base: null
        });
      };

      return resolve;
    } catch (e) {
      return null;
    }
  });

  finalizarFormulario() {
    if (!this.idArticulo || this.idArticulo == 0) {
      this.agregarTienda();
    } else {
      this.actualizarTienda();
    }
  }

  agregarTienda() {
    let data = this.articulosForm.value;

    const formularioDatos = new FormData();
    formularioDatos.append('codigo', data.codigo);
    formularioDatos.append('nombre', data.nombre);
    formularioDatos.append('descripcion', data.descripcion);
    formularioDatos.append('precio', data.precio);
    formularioDatos.append('stock', data.stock);
    formularioDatos.append('IdTienda', this.id);
    if (this.image) {
      formularioDatos.append('Imagen', this.image);
    }

    this.api.insertarArticulo(formularioDatos).subscribe((res: any) => {
      this.articulosForm.reset();
      this.popupVisible = false;
      this.image = null;
      this.image_mostrar = "";
      this.cargarContenido();
    });
  }

  actualizarTienda() {
    let data = this.articulosForm.value;

    const formularioDatos = new FormData();
    formularioDatos.append('IdArticulos', this.idArticulo);
    formularioDatos.append('codigo', data.codigo);
    formularioDatos.append('nombre', data.nombre);
    formularioDatos.append('descripcion', data.descripcion);
    formularioDatos.append('precio', data.precio);
    formularioDatos.append('stock', data.stock);
    formularioDatos.append('IdTienda', this.id);
    if (this.image) {
      formularioDatos.append('Imagen', this.image);
    }

    this.api.actulizarArticulo(formularioDatos).subscribe((res: any) => {
      this.articulosForm.reset();
      this.idArticulo = 0;
      this.popupVisible = false;
      this.image = null;
      this.image_mostrar = "";
      this.cargarContenido();
    });
  }

  eliminarTiendas(event: any) {
    this.api.eliminarArticulo(event.key).subscribe((res: any) => {
      this.cargarContenido();
    });
  }


}
