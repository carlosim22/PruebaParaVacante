import { Component } from '@angular/core';
import { AuthService } from './core/service/auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'app';

  constructor(public auth: AuthService){ }
}
