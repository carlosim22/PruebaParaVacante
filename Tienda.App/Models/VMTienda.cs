﻿namespace Tienda.App.Models
{
    public class VMTienda
    {
        public int IdTienda { get; set; }
        public string? Sucursal { get; set; }
        public string? Direccion { get; set; }
    }
}
