﻿namespace Tienda.App.Models
{
    public class VMCrearArticulo
    {
        public int IdArticulos { get; set; }
        public int? IdTienda { get; set; }
        public string? Codigo { get; set; }
        public string? Nombre { get; set; }
        public string? Descripcion { get; set; }
        public float? Precio { get; set; }
        public IFormFile? Imagen { get; set; }
        public float? Stock { get; set; }
    }

    public class VMArticulo
    {
        public int IdArticulos { get; set; }
        public int? IdTienda { get; set; }
        public string? Codigo { get; set; }
        public string? Nombre { get; set; }
        public string? Descripcion { get; set; }
        public float? Precio { get; set; }
        public string? Imagen { get; set; }
        public float? Stock { get; set; }
    }
}
