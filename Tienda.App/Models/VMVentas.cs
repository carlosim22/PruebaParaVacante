﻿namespace Tienda.App.Models
{
    public class VMVentas
    {
        public int IdVenta { get; set; }
        public int? IdCliente { get; set; }
        public DateTime? FechaVenta { get; set; }
        public float? Subtotal { get; set; }
        public float? Total { get; set; }
    }

    public class VMVentasArticulos
    {
        public int IdVentaArticulos { get; set; }
        public int? IdVenta { get; set; }
        public int? IdArticulo { get; set; }
        public float? Precio { get; set; }
        public float? Cantidad { get; set; }
    }
}
