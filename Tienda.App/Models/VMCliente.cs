﻿namespace Tienda.App.Models
{
    public class VMCliente
    {
        public int IdCliente { get; set; }
        public string? Nombre { get; set; }
        public string? Apellidos { get; set; }
        public string? Domicilio { get; set; }
        public string? Correo { get; set; }
        public string? Contrasenia { get; set; }
    }

    public class VMLogin
    {
        public string? user { get; set; }
        public string? password { get; set; }
    }

    public class VMClienteLogin{
        public int IdCliente { get; set; }
        public string? Nombre { get; set; }
        public string? token { get; set; }
    }
}
