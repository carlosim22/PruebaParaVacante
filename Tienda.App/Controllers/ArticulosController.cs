﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Tienda.App.Models;
using Tienda.BLL.Service;
using Tienda.Models;

namespace Tienda.App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArticulosController : ControllerBase
    {
        private readonly IArticuloService _articuloService;

        public ArticulosController(IArticuloService articuloService)
        {
            _articuloService = articuloService;
        }

        [HttpGet]
        public async Task<IActionResult> obtenerArticulosTiendas(int id_tienda)
        {
            List<Articulo> tiendas = await _articuloService.GetAllActivos(id_tienda);

            return StatusCode(StatusCodes.Status200OK, tiendas);
        }

        [HttpPost]
        public async Task<IActionResult> insertarTiendas([FromForm] VMCrearArticulo tienda)
        {
            string guidImage = null;
            if (tienda.Imagen != null)
            {
                string pathImage = @"Public\Imagenes";
                string ficherosImagenes = Path.GetFullPath(pathImage);
                guidImage = Guid.NewGuid().ToString() + tienda.Imagen.FileName;
                string ruta_final = Path.Combine(ficherosImagenes, guidImage);
                tienda.Imagen.CopyTo(new FileStream(ruta_final, FileMode.Create));
            }

            Articulo NuevoModelo = new Articulo()
            {
                IdTienda = tienda.IdTienda,
                Codigo = tienda.Codigo,
                Nombre = tienda.Nombre,
                Descripcion = tienda.Descripcion,
                Precio = tienda.Precio,
                Imagen = guidImage,
                Stock = tienda.Stock,
                ActivoInactivo = true
            };

            bool respuesta = await _articuloService.Insert(NuevoModelo);

            return StatusCode(StatusCodes.Status200OK, respuesta);
        }

        [HttpPut]
        public async Task<IActionResult> actualizarTiendas([FromForm] VMCrearArticulo tienda)
        {
            string guidImage = null;
            if (tienda.Imagen != null)
            {
                string pathImage = @"Public\Imagenes";
                string ficherosImagenes = Path.GetFullPath(pathImage);
                guidImage = Guid.NewGuid().ToString() + tienda.Imagen.FileName;
                string ruta_final = Path.Combine(ficherosImagenes, guidImage);
                tienda.Imagen.CopyTo(new FileStream(ruta_final, FileMode.Create));
            }

            Articulo NuevoModelo = new Articulo()
            {
                IdArticulos = tienda.IdArticulos,
                IdTienda = tienda.IdTienda,
                Codigo = tienda.Codigo,
                Nombre = tienda.Nombre,
                Descripcion = tienda.Descripcion,
                Precio = tienda.Precio,
                Imagen = guidImage,
                Stock = tienda.Stock,
                ActivoInactivo = true
            };

            bool respuesta = await _articuloService.Update(NuevoModelo);

            return StatusCode(StatusCodes.Status200OK, respuesta);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> eliminarTiendas(int id)
        {
            bool respuesta = await _articuloService.Delete(id);

            return StatusCode(StatusCodes.Status200OK, respuesta);
        }
    }
}
