﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using Tienda.App.Models;
using Tienda.BLL.Service;
using Tienda.Models;

namespace Tienda.App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly IClientesService _clienteService;
        private readonly IConfiguration _config;
        byte[] salt = RandomNumberGenerator.GetBytes(128 / 8);

        public LoginController(IClientesService clienteService, IConfiguration config)
        {
            _clienteService = clienteService;
            _config = config;
        }

        [HttpPost]
        public async Task<IActionResult> login([FromBody] VMLogin login)
        {
            Cliente cliente = await _clienteService.GetLogin(login.user);
            string contrasenia = Encryptacion.DecryptMD5(cliente.Contrasenia);

            if (cliente != null && contrasenia == login.password)
            {
                var token = GenerateToken(cliente);

                VMClienteLogin result = new VMClienteLogin() {
                    IdCliente = cliente.IdCliente,
                    Nombre = cliente.Nombre,
                    token = token
                };

                return StatusCode(StatusCodes.Status200OK, result);
            }
            else
            {
                return StatusCode(StatusCodes.Status400BadRequest, "No existen las credenciales");
            }


        }

        private string GenerateToken(Cliente cliente)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credenciales = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[]
            {
                new Claim("idCliente", cliente.IdCliente.ToString()),
                new Claim(ClaimTypes.GivenName, cliente.Nombre),
                new Claim(ClaimTypes.Surname, cliente.Apellidos),
                new Claim(ClaimTypes.Email, cliente.Correo)
            };

            var token = new JwtSecurityToken(
                _config["Jwt:Issuer"],
                _config["Jwt:Audience"],
                claims,
                signingCredentials: credenciales
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
