﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Tienda.App.Models;
using Tienda.BLL.Service;
using Tienda.Models;

namespace Tienda.App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientesController : ControllerBase
    {
        private readonly IClientesService _clienteService;

        public ClientesController(IClientesService clienteService)
        {
            _clienteService = clienteService;
        }

        [HttpPost]
        public async Task<IActionResult> insertarCliente([FromBody] VMCliente cliente)
        {

            Cliente NuevoModelo = new Cliente()
            {
                Nombre = cliente.Nombre,
                Apellidos = cliente.Apellidos,
                Domicilio = cliente.Domicilio,
                Correo = cliente.Correo,
                Contrasenia = Encryptacion.GetMD5(cliente.Contrasenia),
                ActivoInactivo = true
            };

            bool respuesta = await _clienteService.Insert(NuevoModelo);

            return StatusCode(StatusCodes.Status200OK, respuesta);
        }
    }
}
