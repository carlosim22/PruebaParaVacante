﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Tienda.App.Models;
using Tienda.BLL.Service;
using Tienda.Models;

namespace Tienda.App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TiendaController : ControllerBase
    {
        private readonly ITiendaService _tiendaService;

        public TiendaController(ITiendaService tiendaService)
        {
            _tiendaService = tiendaService;
        }

        [HttpGet]
        public async Task<IActionResult> obtenerTiendas()
        {
            List<Tiendum> tiendas = await _tiendaService.GetAllActivos();

            return StatusCode(StatusCodes.Status200OK, tiendas);
        }

        [HttpPost]
        public async Task<IActionResult> insertarTiendas([FromBody] VMTienda tienda)
        {
            Tiendum NuevoModelo = new Tiendum()
            {
                Sucursal = tienda.Sucursal,
                Direccion = tienda.Direccion,
                ActivoInactivo = true
            };

            bool respuesta = await _tiendaService.Insert(NuevoModelo);
    
            return StatusCode(StatusCodes.Status200OK, respuesta);
        }

        [HttpPut]
        public async Task<IActionResult> actualizarTiendas([FromBody] VMTienda tienda)
        {
            Tiendum NuevoModelo = new Tiendum()
            {
                IdTienda = tienda.IdTienda,
                Sucursal = tienda.Sucursal,
                Direccion = tienda.Direccion,
                ActivoInactivo = true
            };

            bool respuesta = await _tiendaService.Update(NuevoModelo);

            return StatusCode(StatusCodes.Status200OK, respuesta);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> eliminarTiendas(int id)
        {
            bool respuesta = await _tiendaService.Delete(id);

            return StatusCode(StatusCodes.Status200OK, respuesta);
        }
    }
}
