﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Tienda.App.Models;
using Tienda.BLL.Service;
using Tienda.Models;

namespace Tienda.App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VentasArticulosController : ControllerBase
    {
        private readonly IVentasArticulosService _ventasArticulosService;

        public VentasArticulosController(IVentasArticulosService ventasArticulosService)
        {
            _ventasArticulosService = ventasArticulosService;
        }

        [HttpGet]
        public async Task<IActionResult> obtenerVentasArticulos(int id_venta)
        {
            IQueryable<VentasArticulo> vetasArticulos = await _ventasArticulosService.GetAll();
            List<VentasArticulo> articulo = vetasArticulos
                                            .Include(VAr => VAr.IdArticuloNavigation)
                                            .Include(VarArt => VarArt.IdArticuloNavigation.IdTiendaNavigation)
                                            .Where(x => x.IdVenta == id_venta).ToList();

            return StatusCode(StatusCodes.Status200OK, articulo);
        }

        [HttpPost]
        public async Task<IActionResult> insertarVentasArticulos([FromBody] Object articuloVenta)
        {
            dynamic DatosJson = JsonConvert.DeserializeObject<List<VMVentasArticulos>>(articuloVenta.ToString());

            bool respuesta = false;

            foreach (VMVentasArticulos item in DatosJson)
            {
                VentasArticulo NuevoModelo = new VentasArticulo()
                {
                    IdArticulo = item.IdArticulo,
                    IdVenta = item.IdVenta,
                    Precio = item.Precio,
                    Cantidad = item.Cantidad
                };

                respuesta = await _ventasArticulosService.Insert(NuevoModelo);
            }

            return StatusCode(StatusCodes.Status200OK, respuesta);
        }
    }
}
