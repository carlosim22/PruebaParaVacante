﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Tienda.App.Models;
using Tienda.BLL.Service;
using Tienda.Models;

namespace Tienda.App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VentasController : ControllerBase
    {
        private readonly IVentasService _ventasService;

        public VentasController(IVentasService ventasService)
        {
            _ventasService = ventasService;
        }

        [HttpGet]
        public async Task<IActionResult> obtenerTiendas(int id_cliente)
        {
            IQueryable<Venta> tiendas = await _ventasService.GetAll();
            List<Venta> ventas = tiendas.Where(x => x.IdCliente == id_cliente).ToList();

            return StatusCode(StatusCodes.Status200OK, ventas);
        }

        [HttpPost]
        public async Task<IActionResult> insertarTiendas([FromBody] VMVentas venta)
        {
            Venta NuevoModelo = new Venta()
            {
                IdCliente = venta.IdCliente,
                FechaVenta = DateTime.Now,
                Subtotal = venta.Subtotal,
                Total = venta.Total,
            };

            bool respuesta = await _ventasService.Insert(NuevoModelo);

            IQueryable<Venta> tiendas = await _ventasService.GetAll();
            Venta ventas = tiendas.Where(x => x.IdCliente == venta.IdCliente).OrderBy(x => x.IdVenta).Last();

            return StatusCode(StatusCodes.Status200OK, ventas);
        }
    }
}
