﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tienda.DAL.Repositorio;
using Tienda.Models;

namespace Tienda.BLL.Service
{
    public class VentasService : IVentasService
    {

        public readonly IGenericRepository<Venta> _VentasRepository;

        public VentasService(IGenericRepository<Venta> VentasRepo)
        {
            _VentasRepository = VentasRepo;
        }

        public async Task<bool> Delete(int id)
        {
            return await _VentasRepository.Delete(id);
        }

        public async Task<Venta> Get(int id)
        {
            return await _VentasRepository.Get(id);
        }

        public async Task<IQueryable<Venta>> GetAll()
        {
            return await _VentasRepository.GetAll();
        }

        public async Task<List<Venta>> GetAllCliente(int id)
        {
            IQueryable<Venta> queryTiendaSql = await _VentasRepository.GetAll();
            List<Venta> tienda = queryTiendaSql.Where(x => x.IdCliente == id).ToList();
            return tienda;
        }

        public async Task<bool> Insert(Venta modelo)
        {
            return await _VentasRepository.Insert(modelo);
        }

        public async Task<bool> Update(Venta modelo)
        {
            return await _VentasRepository.Update(modelo);
        }
    }
}
