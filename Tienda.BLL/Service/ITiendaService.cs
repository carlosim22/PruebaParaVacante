﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tienda.Models;

namespace Tienda.BLL.Service
{
    public interface ITiendaService
    {
        Task<bool> Insert(Tiendum modelo);
        Task<bool> Update(Tiendum modelo);
        Task<bool> Delete(int id);
        Task<Tiendum> Get(int id);
        Task<IQueryable<Tiendum>> GetAll();

        Task<List<Tiendum>> GetAllActivos();
    }
}
