﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tienda.Models;

namespace Tienda.BLL.Service
{
    public interface IArticuloService
    {
        Task<bool> Insert(Articulo modelo);
        Task<bool> Update(Articulo modelo);
        Task<bool> Delete(int id);
        Task<Articulo> Get(int id);
        Task<IQueryable<Articulo>> GetAll();

        Task<List<Articulo>> GetAllActivos(int id);
    }
}
