﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tienda.DAL.DataContext;
using Tienda.DAL.Repositorio;
using Tienda.Models;

namespace Tienda.BLL.Service
{
    public class ArticuloService : IArticuloService
    {
        public readonly IGenericRepository<Articulo> _articuloRepository;

        public ArticuloService(IGenericRepository<Articulo> context)
        {
            _articuloRepository = context;
        }

        public async Task<bool> Delete(int id)
        {
            return await _articuloRepository.Delete(id);
        }

        public async Task<Articulo> Get(int id)
        {
            return await _articuloRepository.Get(id);
        }

        public async Task<IQueryable<Articulo>> GetAll()
        {
            return await _articuloRepository.GetAll();
        }

        public async Task<List<Articulo>> GetAllActivos(int id)
        {
            IQueryable<Articulo> queryTiendaSql = await _articuloRepository.GetAll();
            List<Articulo> tienda = queryTiendaSql.Where(x => x.ActivoInactivo == true && x.IdTienda == id).ToList();
            return tienda;
        }

        public async Task<bool> Insert(Articulo modelo)
        {
            return await _articuloRepository.Insert(modelo);
        }

        public async Task<bool> Update(Articulo modelo)
        {
            return await _articuloRepository.Update(modelo);
        }
    }
}
