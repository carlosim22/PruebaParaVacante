﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tienda.Models;

namespace Tienda.BLL.Service
{
    public interface IClientesService
    {
        Task<bool> Insert(Cliente modelo);
        Task<bool> Update(Cliente modelo);
        Task<bool> Delete(int id);
        Task<Cliente> Get(int id);
        Task<IQueryable<Cliente>> GetAll();

        Task<Cliente> GetLogin(string correo);
    }
}
