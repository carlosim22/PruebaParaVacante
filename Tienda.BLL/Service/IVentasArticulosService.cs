﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tienda.Models;

namespace Tienda.BLL.Service
{
    public interface IVentasArticulosService
    {
        Task<bool> Insert(VentasArticulo modelo);
        Task<bool> Update(VentasArticulo modelo);
        Task<bool> Delete(int id);
        Task<VentasArticulo> Get(int id);
        Task<IQueryable<VentasArticulo>> GetAll();
    }
}
