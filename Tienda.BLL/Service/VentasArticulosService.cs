﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Tienda.DAL.Repositorio;
using Tienda.Models;

namespace Tienda.BLL.Service
{
    public class VentasArticulosService : IVentasArticulosService
    {
        public readonly IGenericRepository<VentasArticulo> _VentasArticulosRepository;

        public VentasArticulosService(IGenericRepository<VentasArticulo> VentasArticulosRepo)
        {
            _VentasArticulosRepository = VentasArticulosRepo;
        }

        public async Task<bool> Delete(int id)
        {
            return await _VentasArticulosRepository.Delete(id);
        }

        public async Task<VentasArticulo> Get(int id)
        {
            return await _VentasArticulosRepository.Get(id);
        }

        public async Task<IQueryable<VentasArticulo>> GetAll()
        {
            return await _VentasArticulosRepository.GetAll();
        }

        public async Task<bool> Insert(VentasArticulo modelo)
        {
            return await _VentasArticulosRepository.Insert(modelo);
        }

        public async Task<bool> Update(VentasArticulo modelo)
        {
            return await _VentasArticulosRepository.Insert(modelo);
        }
    }
}
