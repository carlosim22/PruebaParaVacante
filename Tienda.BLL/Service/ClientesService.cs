﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tienda.DAL.Repositorio;
using Tienda.Models;

namespace Tienda.BLL.Service
{
    public class ClientesService: IClientesService
    {
        public readonly IGenericRepository<Cliente> _clientesRepository;

        public ClientesService(IGenericRepository<Cliente> clienteRepo)
        {
            _clientesRepository = clienteRepo;
        }

        public async Task<bool> Delete(int id)
        {
            return await _clientesRepository.Delete(id);
        }

        public async Task<Cliente> Get(int id)
        {
            return await _clientesRepository.Get(id);
        }

        public async Task<IQueryable<Cliente>> GetAll()
        {
            return await _clientesRepository.GetAll();
        }

        public async Task<Cliente> GetLogin(string correo)
        {
            IQueryable<Cliente> queryClienteSql = await _clientesRepository.GetAll();
            Cliente cliente = queryClienteSql.Where(x => x.Correo == correo && x.ActivoInactivo == true).FirstOrDefault();
            return cliente;
        }

        public async Task<bool> Insert(Cliente modelo)
        {
            return await _clientesRepository.Insert(modelo);
        }

        public async Task<bool> Update(Cliente modelo)
        {
            return await _clientesRepository.Update(modelo);
        }
    }
}
