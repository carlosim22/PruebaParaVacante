﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tienda.Models;
using Tienda.DAL.Repositorio;

namespace Tienda.BLL.Service
{
    public class TiendaService : ITiendaService
    {
        public readonly IGenericRepository<Tiendum> _tiendaRepository;

        public TiendaService(IGenericRepository<Tiendum> tiendaRepo)
        {
            _tiendaRepository = tiendaRepo;
        }

        public async Task<bool> Delete(int id)
        {
            return await _tiendaRepository.Delete(id);
        }

        public async Task<Tiendum> Get(int id)
        {
            return await _tiendaRepository.Get(id);
        }

        public async Task<IQueryable<Tiendum>> GetAll()
        {
            return await _tiendaRepository.GetAll();
        }

        public async Task<List<Tiendum>> GetAllActivos()
        {
            IQueryable<Tiendum> queryTiendaSql = await _tiendaRepository.GetAll();
            List<Tiendum> tienda = queryTiendaSql.Where(x => x.ActivoInactivo == true).ToList();
            return tienda;
        }

        public async Task<bool> Insert(Tiendum modelo)
        {
            return await _tiendaRepository.Insert(modelo);
        }

        public async Task<bool> Update(Tiendum modelo)
        {
            return await _tiendaRepository.Update(modelo);
        }
    }
}
