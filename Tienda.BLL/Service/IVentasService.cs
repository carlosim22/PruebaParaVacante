﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tienda.Models;

namespace Tienda.BLL.Service
{
    public interface IVentasService
    {
        Task<bool> Insert(Venta modelo);
        Task<bool> Update(Venta modelo);
        Task<bool> Delete(int id);
        Task<Venta> Get(int id);
        Task<IQueryable<Venta>> GetAll();

        Task<List<Venta>> GetAllCliente(int id);
    }
}
