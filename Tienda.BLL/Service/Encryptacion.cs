﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Security.Cryptography;
using System.Text;

namespace Tienda.BLL.Service
{
    public class Encryptacion
    {
        public static string GetMD5(string str)
        {
            string hash = "After all this time";
            byte[] data = UTF8Encoding.UTF8.GetBytes(str);

            MD5 mD5 = MD5.Create();
            TripleDES tripleDes = TripleDES.Create();

            tripleDes.Key = mD5.ComputeHash(UTF8Encoding.UTF8.GetBytes(hash));
            tripleDes.Mode = CipherMode.ECB;

            ICryptoTransform transform = tripleDes.CreateEncryptor();

            byte[] result = transform.TransformFinalBlock(data, 0, data.Length);

            return Convert.ToBase64String(result);
        }

        public static string DecryptMD5(string str)
        {
            string hash = "After all this time";
            byte[] data = Convert.FromBase64String(str);

            MD5 mD5 = MD5.Create();
            TripleDES tripleDes = TripleDES.Create();

            tripleDes.Key = mD5.ComputeHash(UTF8Encoding.UTF8.GetBytes(hash));
            tripleDes.Mode = CipherMode.ECB;

            ICryptoTransform transform = tripleDes.CreateDecryptor();

            byte[] result = transform.TransformFinalBlock(data, 0, data.Length);

            return UTF8Encoding.UTF8.GetString(result);
        }
    }
}
